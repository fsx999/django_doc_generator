# coding=utf-8
"""
Todo
Mise à jour des tables suivantes :
adresse
annee
c_commune

"""
from django.contrib.auth.models import User
from django.db import models



class Voirie(models.Model):
    c_voie = models.CharField(max_length=4)
    l_voie = models.CharField(max_length=30, null=True)
    d_creation = models.DateField()
    d_modification = models.DateField()

    class Meta:
        db_table = u'VOIRIE'


class Pays(models.Model):
    cod_pay = models.CharField(max_length=3, primary_key=True)
    cod_sis_pay = models.CharField(max_length=3)
    lib_pay = models.CharField(max_length=50)
    lic_pay = models.CharField(max_length=20)
    lib_nat = models.CharField(max_length=50)
    tem_ouv_drt_sso_pay = models.CharField(max_length=1)
    tem_en_sve_pay = models.CharField(max_length=1)
    tem_del = models.CharField(max_length=1)
    tem_afl_dec_ind_pay = models.CharField(max_length=1)

    class Meta:
        db_table = u'PAYS'
        ordering = ['lic_pay']

    def __unicode__(self):
        return self.lib_pay


class Annee(models.Model):
    annee = models.IntegerField(primary_key=True)

    class Meta:
        db_table = u'ANNEE'


class Status(models.Model):
    label = models.CharField(max_length=100)
    imposible_charge_cours = models.BooleanField(default=False)

    def __unicode__(self):
        return unicode(self.label)

    class Meta:
        db_table = "STATUT"


class Question(models.Model):
    status = models.ForeignKey(Status)
    label = models.TextField()

    class Meta:
        db_table = "QUESTION"


class Fichier(models.Model):
    question = models.ForeignKey(Question)
    candidature = models.ForeignKey('Candidature')
    fichier = models.FileField(upload_to='data')

    class Meta:
        db_table = 'FICHIER'


class StatusPersonne(models.Model):
    status = models.ForeignKey('Status')
    personne = models.ForeignKey('Individu')
    annee = models.ForeignKey('Annee')

    class Meta:
        db_table = "STATUS_PERSONNE"


class Individu(models.Model):
    GENDER_CHOICES = (
        ('M', 'Homme'),
        ('F', 'Femme'),
    )
    user = models.OneToOneField(User, null=True, related_name='user_personne')
    last_name = models.CharField(u"Nom patronymique", max_length=30, null=True)
    common_name = models.CharField(u"Nom d'époux", max_length=30, null=True,
                                   blank=True)
    first_name1 = models.CharField(u"Prénom", max_length=30)
    first_name2 = models.CharField(u"Deuxième prénom", max_length=30, null=True,
                                   blank=True)
    first_name3 = models.CharField(u"Troisième prénom", max_length=30, null=True,
                                   blank=True)
    personal_email = models.EmailField("Email", unique=True, null=True)
    date_registration_current_year = models.DateTimeField(auto_now_add=True)
    sex = models.CharField(u'sexe', max_length=1, choices=GENDER_CHOICES, null=True)
    birthday = models.DateField('date de naissance', null=True)
    etape = models.CharField("etape", max_length=30, default="personne_info")
    status = models.ManyToManyField('Status', through='StatusPersonne')
    imposible_status = models.ForeignKey('Status', verbose_name=u"status imposible",
                                         related_name="status_imposible_personne",
                                         null=True, blank=True)
    code_pays_birth = models.ForeignKey(
        to=Pays,
        verbose_name=u"Pays de naissance",
        related_name=u"pays_naissance_personne2",
        default=None,
        null=True
    )
    town_birth = models.CharField(
        verbose_name=u'Ville naissance',
        max_length=30,
        null=True,
        blank=True,
    )
    mange_id = models.IntegerField(null=True)

    class Meta:
        db_table = "INDIVIDU"


class IndividuMessage(models.Model):
    individu = models.ForeignKey(Individu)
    date = models.DateField(auto_now=True)
    objet = models.CharField(max_length=200)
    message = models.TextField()

    class Meta:
        db_table = "INDIVIDU_MESSAGE"


class TypeAdresse(models.Model):
    label = models.CharField(max_length=200)

    class Meta:
        db_table = 'TYPE_ADRESSE'


class AdressePersonneModel(models.Model):
    """c'est l'adresse d'individu
    aussi bien l'adresse étrangère que française
    """
    type_adresse = models.ForeignKey(TypeAdresse)
    tel = models.CharField(u'Numero de teléphone :', max_length=15)
    tel_port = models.CharField(u'Numero de teléphone :', max_length=15)
    personne = models.ForeignKey(u'Individu', related_name='adresses')
    code_pays = models.ForeignKey(Pays, verbose_name='Pays :', related_name='apogee_pays')
    adr_adresse = models.CharField(u'Adresse :', max_length=32, null=False)
    adr_adresse_suite = models.CharField(u"Suite de l'adresse :", max_length=32,
                                   null=True, blank=True)

    adr_cp = models.CharField(max_length=5, null=True)
    adr_insee = models.CharField(max_length=5, null=True)

    label_adr_etr = models.CharField(u"Code postal et ville étrangère :",
                                     max_length=32, null=True, blank=True)
    adr_voie = models.ForeignKey(Voirie)
    annee = models.ForeignKey(Annee)

    class Meta:
        db_table = "ADRESSE"


class EC(models.Model):
    class Meta:
        db_table = "ELP"


class Candidature(models.Model):
    ec = models.ForeignKey('Ec')
    personne = models.ForeignKey(Individu)
    autoriser_candidater = models.BooleanField(default=False)
    etape = models.CharField(max_length=20, default="debut_ec")
    code_dossier = models.CharField(max_length=20, primary_key=True)
    cv_file = models.FileField(upload_to="test/", null=True)
    lm_file = models.FileField(upload_to="test/", null=True)
    annee = models.ForeignKey(Annee)
    date = models.DateField(auto_now=True)

    class Meta:
        db_table = "CANDIDATURE"


class EtatOffreEc(models.Model):
    label = models.CharField(max_length=200)

    class Meta:
        db_table = 'ETAT_OFFRE_EC'


class OffreEc(models.Model):
    candidature = models.ForeignKey(Candidature)
    candidature_heure = models.IntegerField()
    etat = models.ForeignKey(EtatOffreEc)
    date = models.DateField(auto_now=True)

    class Meta:
        db_table = " OFFRE_EC"
