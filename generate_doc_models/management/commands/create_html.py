# -*- coding: utf-8 -*-
import shutil

from django.core.management.base import BaseCommand
from optparse import make_option
import importlib
from django.db import connections, models
import sys
import os
import inspect
from django.template.loader import get_template, render_to_string

settings = importlib.import_module(os.environ['DJANGO_SETTINGS_MODULE'])
PROJECT_DIR = os.path.abspath(os.path.join(os.path.dirname(settings.__file__), os.path.pardir))

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('-e',
                    '--exclude',
                    action='store',
                    dest='exclude',
                    type='string',
                    default=[],
                    help='Models to exclude'),
        make_option('-c',
                    '--connection',
                    action='store',
                    dest='connection',
                    type='string',
                    default='default',
                    help='database in database settings'),
        make_option('-i',
                    '--include',
                    action='store',
                    dest='include',
                    type='string',
                    default=[],
                    help='database in database settings'),
        make_option('-d',
                    '--dir',
                    action='store',
                    dest='dir',
                    type='string',
                    default='doc',
                    help='dir for documentation'),
    )
    len_colonne = 80

    def handle(self, *args, **options):
        self.set_up(**options)
        exclude = options['exclude']
        include = options['include']

        if include != []:
            exclude = []
        for module in args:
            name_current_module = '%s.models' % module
            try:
                current_module = sys.modules[name_current_module]
            except KeyError, e:
                raise KeyError(u"Le module %s n'existe pas" % e)

            for name, obj in inspect.getmembers(current_module):
                if inspect.isclass(obj) and issubclass(obj.__class__, models.base.ModelBase)\
                    and not obj._meta.proxy and obj.__module__ == name_current_module and name not in exclude:
                    if include != []:

                        if name in include:
                            self.print_obj_rest(obj)
                    else:
                        self.print_obj_rest(obj)

        os.system('sphinx-build -b html %s %s' % (self.projet, self.projet+'/html'))
        self.set_down()

    def set_down(self):
        shutil.rmtree(self.projet+'/table_sql', True)
        shutil.rmtree(self.projet+'/_static', True)
        shutil.rmtree(self.projet+'/html/.doctrees', True)
        shutil.rmtree(self.projet+'/html/_sources', True)
        os.remove(self.projet+'/conf.py')
        os.remove(self.projet+'/index.rst')

    def create_index_rst(self):
        file = open(self.projet + "/index.rst", 'w')
        file.writelines(render_to_string('generate_doc_models/index.rst'))
        file.close()

    def print_obj_rest(self, obj):
        lignes = []
        for field in obj._meta.local_fields:
            cases = [field.column.upper(), field.db_type(connections['oracle'])]

            if hasattr(field, 'related'):
                cases.append(u"%s => %s" % (":ref:`%s`" % field.related.parent_model._meta.db_table.upper(),
                                            field.rel.field_name.upper()))
            else:
                cases.append("")

            lignes.append(cases)
        context = {'table': obj._meta.db_table.upper()}
        context['len_colonne'] = self.len_colonne
        context['lignes'] = lignes

        if len(obj._meta.local_many_to_many):
            lignes_m2m = []
            for field in obj._meta.local_many_to_many:

                lignes_m2m.append([":ref:`%s`" % field.m2m_db_table(), field.m2m_column_name()])
            context['lignes_m2m'] = lignes_m2m
        file = open(self.projet+'/table_sql/' + obj._meta.db_table.upper()+'.rst', 'w')
        file.writelines(render_to_string('generate_doc_models/tableau/entete_table.rst', context))
        file.close()

    def set_up(self, **options):
        dir = options['dir']
        self.projet = PROJECT_DIR + '/' + dir
        self.create_dirs([
            self.projet,
            self.projet+'/table_sql',
            self.projet+'/html',
            self.projet+'/_static',

        ])
        self.create_conf()
        self.create_index_rst()

    def create_conf(self):
        file = open(self.projet+'/conf.py', 'w')
        file.writelines(render_to_string('generate_doc_models/conf.py'))
        file.close()

    def create_dirs(self, dirs):
        for dir in dirs:
            try:
                os.mkdir(dir)
            except OSError:
                pass
