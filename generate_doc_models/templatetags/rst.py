# coding=utf-8

from django import template

register = template.Library()


@register.filter
def do_case(value, arg):
    value = str(value)
    return value + " " * (arg-len(value)+1)


@register.simple_tag
def colonne_tableau(value, nb):
    chaine = ("=" * value + "  ")*nb
    return chaine
